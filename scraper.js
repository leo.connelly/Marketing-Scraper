const puppeteer = require("puppeteer");
const clientID = "&client_id=MjE2ODY2MDF8MTYxNzkxNjczMS4wMDQzMjY";
const fetch = require("node-fetch");

//Get all the venues that are very popular in USA
async function getAllVenues() {
  var pageNumber = 1;

  const urlFetch =
    "https://api.seatgeek.com/2/venues?per_page=5000&page=" +
    pageNumber +
    "&country=US&client_id=MjE2ODY2MDF8MTYxNzkxNjczMS4wMDQzMjY";
  //Keep in mind the dummy data is an array of facilities
  console.log(urlFetch);
  const response = await fetch(urlFetch).then((res) => res.json());
  //console.log(response);

  return response;
}

//Create an array of all the venues
async function createArrayOfAllVenueNames(fetchResponse) {
  console.log(fetchResponse.venues.length);
  var tempArray = [];

  for (var i = 0; i < fetchResponse.venues.length; i++) {
    if (fetchResponse.venues[i].score > 0.9) {
      //   console.log(fetchResponse.venues[i].name_v2);
      //   console.log(fetchResponse.venues[i].score);
      const venue = {
        name: fetchResponse.venues[i].name_v2,
        score: fetchResponse.venues[i].score,
      };

      tempArray.push(venue);
    }
  }
  console.log("Length is");
  console.log(tempArray.length);
  console.log("Length is");
}

//Scrape URLs: This function returns an array of ParkMe Urls that we link us to pages with
//the operator names
async function scrapeURLs() {
  var venueName = "SofiStadium";
  const browser = await puppeteer.launch({
    //args: ["--no-sandbox"],
    headless: true,
    defaultViewport: null,
  });
  const page = await browser.newPage();
  const url = "https://www.parkme.com/map?q=" + venueName;
  await page.goto(url);

  await page.waitFor(".featured_lot_entry");

  console.log(url);

  const hrefs = await page.evaluate(() =>
    Array.from(
      document.querySelectorAll(
        ".left.btn.btn-primary.btn-small.fle_reserve.compare-res-btn"
      ),
      (a) => a.getAttribute("href")
    )
  );
  console.log(hrefs);
  await browser.close();

  return hrefs;
}

async function scrapePages(urls) {
  const browser = await puppeteer.launch({
    //args: ["--no-sandbox"],
    headless: true,
    defaultViewport: null,
  });

  const page = await browser.newPage();

  for (facilityURL in urls) {
    var url = "https://www.parkme.com";
    url = url + urls[facilityURL];
    console.log(url);

    try {
      await page.goto(url);
      await page.waitFor(".module-table-group.operator-table");

      const result = await page.$$eval(
        ".module-table-group.operator-table tr",
        (rows) => {
          return Array.from(rows, (row) => {
            const columns = row.querySelectorAll("td");
            return Array.from(columns, (column) => column.innerText);
          });
        }
      );
      const resultObj = arr2obj(result);
      delete resultObj.undefined;
      console.log(resultObj);
    } catch {
      console.log("error");
    }
  }
}

function arr2obj(arr) {
  // Create an empty object
  let obj = {};

  arr.forEach((v) => {
    // Extract the key and the value
    let key = v[0];
    let value = v[1];

    // Add the key and value to
    // the object
    obj[key] = value;
  });

  // Return the object
  return obj;
}

async function runInOrder() {
  // const venueResponse = await getAllVenues();
  // const filteredVenues = await createArrayOfAllVenueNames(venueResponse);
  const urlArray = await scrapeURLs();
  const operatorNames = await scrapePages(urlArray);
}

runInOrder();
